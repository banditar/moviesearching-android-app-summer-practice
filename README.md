Android application that shows movies and their descriptions. User can search movies by their titles.

Demonstration:\
https://youtu.be/hnTzNLWPK6I

Developed in Android Studio, in Kotlin programming language.\
Uses:
- Retrofit for fetching the data from OMDB Api (https://www.omdbapi.com/).
- Picassa for displaying the images.

Made by: Jánosi József-Hunor, during my summer internship at REEA SRL, in 2020, while I was in my second year of my bachelor's degree.
