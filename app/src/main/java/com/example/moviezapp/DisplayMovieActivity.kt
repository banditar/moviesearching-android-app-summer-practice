package com.example.moviezapp

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.moviezapp.data.API
import com.example.moviezapp.data.response.Movie
import com.squareup.picasso.Picasso
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DisplayMovieActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_display_movie)

        val myImbdID = intent.getStringExtra(imdbID)

        // retrofit get movie
        val api = API()
        api.getMovie(id= myImbdID!!)
            .enqueue(object: Callback<Movie> {
                override fun onFailure(call: Call<Movie>, t: Throwable) {
                    Log.d(TAG, "Error: $t")
                }

                override fun onResponse(call: Call<Movie>, response: Response<Movie>) {
                    if (response.isSuccessful) {
                        val result = response.body()

                        // if got response
                        if (result?.title.toString() != "null") {

                            val layout = findViewById<LinearLayout>(R.id.movieSpecsLayout)

                            // title
                            val title = findViewById<TextView>(R.id.movieTitle)
                            title.text = result?.title


                            // poster
                            val poster = ImageView(this@DisplayMovieActivity)
                            layout.addView(poster)
//                            val url = POSTER_BASE_URL.plus(result?.imdbID)
                            val url = result?.poster

                            val width = 0
                            val height = 1000

                            Picasso.get()
                                .load(url)
                                .placeholder(R.drawable.poster_failed)
                                .resize(width, height)
                                .into(poster)

                            // specs
                            // adds a TextView with 'Title' and its meaning
                            addSpecView(layout, result?.plot.toString(), "Plot")
                            addSpecView(layout, result?.genre.toString(), "Genre")
                            addSpecView(layout, result?.actors.toString(), "Actors")
                            addSpecView(layout, result?.year.toString(), "Year")
                            addSpecView(layout, result?.director.toString(), "Director")
                            addSpecView(layout, result?.language.toString(), "Language")
                            addSpecView(layout, result?.awards.toString(), "Awards")
                            addSpecView(layout, result?.runtime.toString(), "Runtime")
                            addSpecView(layout, result?.imdbRating.toString(), "ImdbRating")
                        }
                    }
                }
            })
    }

    fun addSpecView(layout: LinearLayout, resultAttr: String, resultString: String) {
        if (resultAttr != "N/A") {
            val line = View(this@DisplayMovieActivity)
            line.layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                6
            )
            line.setBackgroundColor(Color.parseColor("#c0c0c0"))
            layout.addView(line)

            val specText = TextView(this@DisplayMovieActivity)
            specText.run {
                text = resultString
                textSize = 18F
                setTextColor(Color.parseColor("#000000"))
            }
            layout.addView(specText)

            val spec = TextView(this@DisplayMovieActivity)
            spec.text = resultAttr
            layout.addView(spec)
        }
    }
}