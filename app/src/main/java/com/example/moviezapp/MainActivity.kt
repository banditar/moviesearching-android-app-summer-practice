package com.example.moviezapp

import android.content.Intent
import android.graphics.Point
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.example.moviezapp.data.API
import com.example.moviezapp.data.response.Movie
import com.example.moviezapp.data.response.Search
import com.example.moviezapp.data.response.TitleSearch
import com.squareup.picasso.Picasso
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.random.Random


val BASE_URL = "https://www.omdbapi.com/"
val POSTER_BASE_URL = "https://img.omdbapi.com/?apikey=4188fe2c&i="
val TAG = "*****TAGSTUFF******"

const val imdbID = ""

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        displayStarterMovies()
    }

    fun displayStarterMovies() {
        // for refresh, remove all on layout
        val layout = findViewById<GridLayout>(R.id.mainGrid)

        val api = API()

        val maxLoop = 6

        val list: MutableList<String> = mutableListOf()

        val wordList = listOf<String>("star", "game of", "one day", "12 angry men", "night", "yes man", "that sugar film", "the godfather", "irishman", "batman", "spider-verse", "no strings attached", "usual", "get", "once upon a time", "christopher robin", "friends", "dark", "bad poems", "dead poets", "moscow square", "shawshank", "twilight", "potter")

        val start = 0
        val end = wordList.lastIndex

        val randList = mutableListOf<Int>()
        randList.add(Random.nextInt(start, end))
        for (i in 1 until maxLoop) {
            var newRand = Random.nextInt(start, end)
            while (newRand in randList) {
                newRand = Random.nextInt(start, end)
            }
            randList.add(newRand)
        }

        for (i in 0 until maxLoop) {
            val title = wordList[randList[i]]

            api.getMovie(title=title)
                .enqueue(object: Callback<Movie> {
                    override fun onFailure(call: Call<Movie>, t: Throwable) {
                        Log.d(TAG, "Error: $t")
                    }

                    override fun onResponse(call: Call<Movie>, response: Response<Movie>) {
                        if (response.isSuccessful) {
                            val result = response.body()

                            if (result?.title != null) {
                                // if this movie was already displayed
                                if (result.imdbID !in list) {
                                    list.add(result.imdbID)

                                    // poster
                                    val poster = ImageButton(this@MainActivity)
                                    // onClick
                                    poster.setOnClickListener {
                                        movieTitleOnClick(result)
                                    }

                                    layout.addView(poster)

                                    val url = result.poster

                                    Picasso.get()
                                        .load(url)
                                        .placeholder(R.drawable.poster_failed)
                                        .into(poster, object : com.squareup.picasso.Callback {
                                            override fun onSuccess() {
                                            }

                                            // if can't find poster, show title as button
                                            override fun onError(e: Exception?) {
                                                layout.removeView(poster)

                                                val newTitle = Button(this@MainActivity)
                                                newTitle.text = result.title

                                                val displayMetrics = DisplayMetrics()
                                                windowManager.defaultDisplay
                                                    .getMetrics(displayMetrics)
                                                val textHeight = displayMetrics.heightPixels
                                                val textWidth = displayMetrics.widthPixels

                                                newTitle.layoutParams = ViewGroup.LayoutParams(
                                                    textWidth / 3,
                                                    ViewGroup.LayoutParams.WRAP_CONTENT
                                                )
                                                layout.addView(newTitle)

                                                // onClick
                                                newTitle.setOnClickListener {
                                                    movieTitleOnClick(result)
                                                }
                                            }
                                        })
                                }
                            }
                        }
                    }
                })
        }
    }

    fun searchMovie(view: View) {
        val gridLayout = findViewById<GridLayout>(R.id.mainGrid)
        val linearLayout = findViewById<LinearLayout>(R.id.LinearLayout)
        refreshLayout()

        // add loader
        val loader = ProgressBar(this)
        linearLayout.addView(loader)

        val api = API()

        val title = findViewById<EditText>(R.id.movieTitle).text.toString()

        api.getMoviesByTitle(title=title.trim())
            .enqueue(object: Callback<TitleSearch> {
                override fun onFailure(call: Call<TitleSearch>, t: Throwable) {
                    Log.d(TAG, "Error: $t")
                }

                override fun onResponse(call: Call<TitleSearch>, response: Response<TitleSearch>) {
                    if (response.isSuccessful) {
                        val result = response.body()

                        if (result?.search != null) {
                            for (movie in result.search) {
                                // poster
                                val poster = ImageButton(this@MainActivity)
                                // onClick
                                poster.setOnClickListener{
                                    movieTitleOnClick(movie)
                                }

                                gridLayout.addView(poster)

//                                val url = POSTER_BASE_URL.plus(movie.imdbID)
                                val url = movie.poster

                                val display = windowManager.defaultDisplay
                                val size = Point()
                                display.getSize(size)
//                                val width = (size.x) / 2
//                                val height = ((size.y) / 2.2).roundToInt()

                                val width = 0
                                val height = 750

                                Picasso.get()
                                    .load(url)
                                    .placeholder(R.drawable.poster_failed)
//                                    .resize(width, height)
                                    .into(poster, object : com.squareup.picasso.Callback {
                                        override fun onSuccess() {
                                        }

                                        // if can't find poster, show title as button
                                        override fun onError(e: Exception?) {
                                            gridLayout.removeView(poster)

                                            val newTitle = Button(this@MainActivity)
                                            newTitle.text = movie.title

                                            val displayMetrics = DisplayMetrics()
                                            windowManager.defaultDisplay
                                                .getMetrics(displayMetrics)
                                            val textHeight = displayMetrics.heightPixels
                                            val textWidth = displayMetrics.widthPixels

                                            newTitle.layoutParams = ViewGroup.LayoutParams(textWidth / 3 ,ViewGroup.LayoutParams.WRAP_CONTENT)
                                            gridLayout.addView(newTitle)

                                            // onClick
                                            newTitle.setOnClickListener{
                                                movieTitleOnClick(movie)
                                            }
                                        }
                                    })
                            }
                        }
                        else {
                            // add textview
                            val notFoundTextView = TextView(this@MainActivity)
                            notFoundTextView.text = getString(R.string.notFoundText)
                            gridLayout.addView(notFoundTextView)
                        }
                    }

                    // remove loader
                    linearLayout.removeView(loader)
                }
            })
    }

    fun refreshLayout() {
        val layout = findViewById<LinearLayout>(R.id.LinearLayout)
        val gridLayout = findViewById<GridLayout>(R.id.mainGrid)

        gridLayout.removeAllViewsInLayout()
        layout.removeView(findViewById(R.id.recommendText))
    }

    fun movieTitleOnClick(movie: Search) {
        // new Activity
        val intent = Intent(this@MainActivity, DisplayMovieActivity::class.java).apply {
            putExtra(imdbID, movie.imdbID)
        }
        startActivity(intent)
    }

    fun movieTitleOnClick(movie: Movie) {
        // new Activity
        val intent = Intent(this@MainActivity, DisplayMovieActivity::class.java).apply {
            putExtra(imdbID, movie.imdbID)
        }
        startActivity(intent)
    }
}
