package com.example.moviezapp.data

import com.example.moviezapp.data.response.Movie
import com.example.moviezapp.data.response.TitleSearch
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

val BASE_URL = "http://www.omdbapi.com/"
val API_KEY = "4188fe2c"

interface API {
    @GET(".")
    fun getMovie(
        @Query("i") id: String = "",
        @Query("t") title: String = "",
        @Query("y") year: String = "",
        @Query("type") type: String = "",
        @Query("plot") plot: String = "full",

        @Query("apikey") apikey: String = API_KEY
    ): Call<Movie>

    @GET(".")
    fun getMoviesByTitle(
        @Query("s") title: String,
        @Query("y") year: String = "",
        @Query("type") type: String = "",
        @Query("plot") plot: String = "short",

        @Query("apikey") apikey: String = API_KEY
    ): Call<TitleSearch>

    companion object {
        operator fun invoke(): API {
            return Retrofit.Builder()
                .baseUrl(com.example.moviezapp.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(API::class.java)
        }
    }
}