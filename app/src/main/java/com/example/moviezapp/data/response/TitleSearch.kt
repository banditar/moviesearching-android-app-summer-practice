package com.example.moviezapp.data.response


import com.example.moviezapp.data.response.Search
import com.google.gson.annotations.SerializedName

data class TitleSearch(
    @SerializedName("Response")
    val response: String,
    @SerializedName("Search")
    val search: List<Search>,
    val totalResults: String
)